# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141015212441) do

  create_table "fighters", force: true do |t|
    t.string   "name"
    t.date     "born_on"
    t.string   "nationality"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fights", force: true do |t|
    t.string   "event_name"
    t.string   "event_url"
    t.integer  "fighter1_id"
    t.string   "fighter1_result"
    t.integer  "fighter1_result_elo"
    t.integer  "fighter2_id"
    t.string   "fighter2_result"
    t.integer  "fighter2_result_elo"
    t.date     "fought_on"
    t.string   "location"
    t.string   "referee"
    t.string   "result_method"
    t.string   "result_round"
    t.string   "result_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fighter1_result_elo_count"
    t.integer  "fighter2_result_elo_count"
    t.float    "fighter1_predicted_chance_to_win"
    t.float    "fighter2_predicted_chance_to_win"
    t.string   "fighter1_betting_line"
    t.string   "fighter2_betting_line"
    t.float    "fighter1_betting_return"
    t.float    "fighter2_betting_return"
    t.string   "betting_url"
  end

end
