class AddBettingLinesToFights < ActiveRecord::Migration
  def change
    add_column :fights, :fighter1_betting_line, :string
    add_column :fights, :fighter2_betting_line, :string
    add_column :fights, :fighter1_betting_return, :float
    add_column :fights, :fighter2_betting_return, :float
    add_column :fights, :betting_url, :string
  end
end
