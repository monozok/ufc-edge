class CreateFighters < ActiveRecord::Migration
  def change
    create_table :fighters do |t|
      t.string :name
      t.date :born_on
      t.string :nationality
      t.string :url

      t.timestamps
    end
  end
end
