class AddFighterRatingCountToFights < ActiveRecord::Migration
  def change
    add_column :fights, :fighter1_result_elo_count, :integer
    add_column :fights, :fighter2_result_elo_count, :integer
  end
end
