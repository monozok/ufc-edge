class AddWinPredictionToFights < ActiveRecord::Migration
  def change
    add_column :fights, :fighter1_predicted_chance_to_win, :float
    add_column :fights, :fighter2_predicted_chance_to_win, :float
  end
end
