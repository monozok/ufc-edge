class CreateFights < ActiveRecord::Migration
  def change
    create_table :fights do |t|
      t.string :event_name
      t.string :event_url
      t.integer :fighter1_id
      t.string :fighter1_result
      t.integer :fighter1_result_elo
      t.integer :fighter2_id
      t.string :fighter2_result
      t.integer :fighter2_result_elo
      t.date :fought_on
      t.string :location
      t.string :referee
      t.string :result_method
      t.string :result_round
      t.string :result_time

      t.timestamps
    end
  end
end
