Elo.configure do |config|

  config.k_factor(30) { games_played <= 5 }
  config.default_k_factor = 12
  config.use_FIDE_settings = false

end
