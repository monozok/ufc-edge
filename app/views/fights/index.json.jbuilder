json.array!(@fights) do |fight|
  json.extract! fight, :id, :event_name, :event_url, :fighter1_id, :fighter1_result, :fighter1_result_elo, :fighter2_id, :fighter2_result, :fighter2_result_elo, :fought_on, :location, :referee, :result_method, :result_round, :result_time
  json.url fight_url(fight, format: :json)
end
