json.array!(@fighters) do |fighter|
  json.extract! fighter, :id, :name, :born_on, :nationality, :url
  json.url fighter_url(fighter, format: :json)
end
