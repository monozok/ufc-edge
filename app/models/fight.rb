class Fight < ActiveRecord::Base
  belongs_to :fighter1, foreign_key: "fighter1_id", class_name: "Fighter"
  belongs_to :fighter2, foreign_key: "fighter2_id", class_name: "Fighter"

  def fighter1_actual_return
    actual_return(fighter1_result, fighter1_betting_line)
  end

  def fighter2_actual_return
    actual_return(fighter2_result, fighter2_betting_line)
  end

  def actual_return(fight_result, betting_line)
    if fight_result == 'win'
      if betting_line.to_i < 0
        win_return = 100 / betting_line.to_f.abs
      else
        win_return = betting_line.to_f / 100
      end
    else
      -1
    end
  end
end
