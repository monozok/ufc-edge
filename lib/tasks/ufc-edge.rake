require './app/models/fight'
require './app/models/fighter'
require './lib/ufc_edge/fight_scraper'
require './lib/ufc_edge/betting_line_scraper'
require './lib/ufc_edge/elo_calculator'
require './lib/ufc_edge/betting_calculator'
require './lib/ufc_edge/elo_prediction_evaluator'
require './lib/ufc_edge/betting_prediction_evaluator'

namespace :ufc_edge do

  desc "Do all"
  task :all => :environment do
    Rake::Task["ufc_edge:scrape_fights"].invoke
    Rake::Task["ufc_edge:scrape_betting_lines"].invoke
    Rake::Task["ufc_edge:calculate_elo"].invoke
    Rake::Task["ufc_edge:calculate_betting_returns"].invoke
    Rake::Task["ufc_edge:evaluate_elo_predictions"].invoke
    Rake::Task["ufc_edge:evaluate_betting_predictions"].invoke
  end

  desc "Scrape fights and save new source data"
  task :scrape_fights => :environment do
    fight_scraper = UfcEdge::FightScraper.new
    fight_scraper.scrape_fights
  end

  desc "Scrape betting lines and overwrite source data"
  task :scrape_betting_lines => :environment do
    betting_line_scraper = UfcEdge::BettingLineScraper.new
    betting_line_scraper.scrape_betting_lines
  end

  desc "Calculate and overwrite all elo ratings"
  task :calculate_elo => :environment do
    calculator = UfcEdge::EloCalculator.new
    calculator.calculate!
  end

  desc "Calculate and overwrite all expected returns"
  task :calculate_betting_returns => :environment do
    calculator = UfcEdge::BettingCalculator.new
    calculator.calculate!
  end

  desc "Evaluate Elo predictions"
  task :evaluate_elo_predictions => :environment do
    evaluator = UfcEdge::EloPredictionEvaluator.new
    evaluator.evaluate
  end

  desc "Evaluate betting predictions"
  task :evaluate_betting_predictions => :environment do
    evaluator = UfcEdge::BettingPredictionEvaluator.new
    evaluator.evaluate
  end

end
