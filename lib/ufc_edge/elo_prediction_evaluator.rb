module UfcEdge

  class EloPredictionEvaluator

    def evaluate
      
      total_difference = 0.0
      usable_fights = Fight.where("fighter1_result_elo_count > 4 and fighter2_result_elo_count > 4")

      (1..100).each do |percent|
        probability = percent / 100.0
        wins = usable_fights.where("fighter1_predicted_chance_to_win between #{probability.to_f} and #{probability.to_f + 0.01} and fighter1_result = 'win'").count
        total = usable_fights.where("(fighter2_predicted_chance_to_win between #{probability.to_f} and #{probability.to_f + 0.01}) OR (fighter1_predicted_chance_to_win between #{probability.to_f} and #{probability.to_f + 0.01})").count
        actual_percent_won = (wins.to_f / total.to_f) * 100

        if !actual_percent_won.nan?
          difference = ((percent + 1) - actual_percent_won)
          puts "Difference: #{sprintf('%.1f', difference)}; Predicted win % range: #{percent}-#{percent + 1}; Actual win %: #{actual_percent_won.round(1)}; (W/T: #{wins}/#{total})"
          total_difference += (difference.abs * total)
        end
      end

      puts "Total Difference: #{total_difference.round(1)}"

    end

  end
end