module UfcEdge

  class EloCalculator

    def calculate!
      fighter_map = Fighter.all.inject({}) do |memo, fighter|
        # initialize fighters for rating
        fighter.elo_player = Elo::Player.new

        memo[fighter.id.to_s] = fighter
        memo
      end

      fights = Fight.where("fighter1_result is not null").order("fought_on asc")
      fights.each do |fight|

        player1 = fighter_map[fight.fighter1_id.to_s].elo_player
        player2 = fighter_map[fight.fighter2_id.to_s].elo_player

        fight.fighter1_predicted_chance_to_win = elo_difference_to_predicted_chance_to_win(player1.rating, player2.rating)
        fight.fighter2_predicted_chance_to_win = 1.0 - fight.fighter1_predicted_chance_to_win

        case fight.fighter1_result
        when 'win'
          player1.wins_from(player2)
        when 'loss'
          player1.loses_from(player2)
        when 'draw'
          player1.plays_draw(player2)
        when 'nc'
          # nothing
        end

        fight.fighter1_result_elo = player1.rating
        fight.fighter2_result_elo = player2.rating
        fight.fighter1_result_elo_count = player1.games_played
        fight.fighter2_result_elo_count = player2.games_played
        fight.save

      end
    end

    def elo_difference_to_predicted_chance_to_win(rating1, rating2)
      (1.0 / (1.0 + (10.0 ** ((rating2 - rating1) / 400.0))))
    end
  end
end