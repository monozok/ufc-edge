module UfcEdge

  class BettingPredictionEvaluator

    def evaluate

      puts "Verify negative returns"
      average_actual_return(-1000, 0, 5)

      puts "Vary minimum expected return hurdle"
      (0..100).step(10) do |hurdle|
        average_actual_return(hurdle, 1000, 5)
      end

      puts "Vary minimum fights"
      (0..20).each do |trials|
        average_actual_return(20, 1000, trials)
      end

      puts "Conclusion: NO GO!"
      
    end

    def average_actual_return(lower_expected, upper_expected, minimum_trials)
      fight_pool = Fight.where("fought_on > '2008-01-01' and fighter1_result_elo_count > #{minimum_trials} and fighter2_result_elo_count > #{minimum_trials}")
      fights1 = fight_pool.where("fighter1_betting_return between #{lower_expected / 100.0} and #{upper_expected / 100.0}")
      fights2 = fight_pool.where("fighter2_betting_return between #{lower_expected / 100.0} and #{upper_expected / 100.0}")
      return if fights1.empty? && fights2.empty?
      total_return = fights1.reduce(0) do |memo, fight|
        memo + fight.fighter1_actual_return
      end
      total_return = fights2.reduce(total_return) do |memo, fight|
        memo + fight.fighter2_actual_return
      end
      fight_count = fights1.size + fights2.size
      average_return = total_return / fight_count * 100.0
      puts "Expected between #{lower_expected}% and #{upper_expected}%, got: #{average_return.round(1)} over #{fight_count} fights, minimum_trials: #{minimum_trials}"
    end

  end
end