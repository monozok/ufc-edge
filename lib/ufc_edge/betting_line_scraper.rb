module UfcEdge

  class BettingLineScraper

    EVENTS_URL = 'https://www.bestfightodds.com/search?query=ufc'
    EVENT_CACHE_PATH = 'tmp/betting_events'

    def initialize
      @a = Mechanize.new
    end

    def scrape_betting_lines
      assign_betting_urls_to_fights
      event_urls = Fight.select("betting_url").distinct.map(&:betting_url)
      events = cached_get_events(event_urls)
      assign_betting_lines_to_fights(events)
    end

    def assign_betting_urls_to_fights
      events_page = @a.get(EVENTS_URL)
      numbered_ufc_fights = Fight.select { |fight| fight.event_name.match(/\AUFC\s+\d+ /) }
      numbered_ufc_fights.each do |fight|
        fight_number = fight.event_name.match(/\AUFC\s+(\d+)/)[1]
        event_link = events_page.link_with(text: Regexp.new("UFC\s+#{fight_number}:"))
        if event_link
          fight.betting_url = event_link.href
          fight.save
        end
      end
    end

    def cached_get_events(event_urls, recent_urls=[])
      events = load_cached_events
      @a.get(EVENTS_URL) # hack so we can use relative urls at bestfightodds.com again
      get_urls = event_urls - events.keys # don't download if it's already cached
      get_urls = get_urls + recent_urls # get recent events even if they're cached
      get_urls = get_urls.uniq.compact
      get_urls.each_with_index do |url, i|
        page = @a.get(url)
        save_page page, url
        events[url] = page
        puts "Got url #{i+1} of #{get_urls.size}." if ((i+1) % 20 == 0)
      end
      events
    end

    def load_cached_events
      events = {}
      source_files_glob_pattern = File.join(Rake.application.original_dir, EVENT_CACHE_PATH, '*.html')
      Dir.glob(source_files_glob_pattern) do |file_path|
        page = @a.get "file://#{file_path}"
        file_name = File.basename(file_path, ".html")
        event_url = "/events/#{file_name}"
        puts "Loading cached event page #{file_name}"
        events[event_url] = page
      end
      events
    end

    def save_page page, url
      FileUtils.mkpath EVENT_CACHE_PATH
      file_name = url.gsub('/events/', '') + '.html'
      save_path = File.join(Rake.application.original_dir, EVENT_CACHE_PATH, file_name)
      body = page.body.encode('utf-8', :invalid => :replace, :undef => :replace, :replace => '_')
      puts "Saving #{save_path}"
      File.open(save_path, 'w') { |f| f.write(body) }
    end

    def assign_betting_lines_to_fights(events)
      events.each do |event_url, event_page|
        fights = Fight.where(:betting_url => event_url)
        next if fights.empty?
        
        fights.each do |fight|
          fight.fighter1_betting_line = extract_betting_line_for_fighter(event_page, fight.fighter1)
          fight.fighter2_betting_line = extract_betting_line_for_fighter(event_page, fight.fighter2)
          fight.save
        end
      end
    end

    def extract_betting_line_for_fighter(event_page, fighter)
      fighter_node = event_page.at(".odds-table th:contains(\"#{fighter.name}\")")
      unless fighter_node
        puts "Did not find #{fighter.name} in .odds-table.  Skipping."
        return nil
      end
      fighter_node.parent.at(".bestbet").try(:text)
    end

  end
end
