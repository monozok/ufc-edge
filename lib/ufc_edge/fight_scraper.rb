module UfcEdge

  class FightScraper

    EVENTS_URL = 'http://www.sherdog.com/organizations/Ultimate-Fighting-Championship-2'
    EVENT_CACHE_PATH = 'tmp/events'

    def scrape_fights
      @a = Mechanize.new
      events_page = @a.get(EVENTS_URL)
      found_event_urls = events_page.links_with(href: /\/events\/UFC/).map(&:href)
      new_event_urls = found_event_urls - existing_event_urls
      events = cached_get_events(new_event_urls, recent_event_urls)

      fights = []
      events.each do |event_url, event_page|
        fights += extract_fights(event_url, event_page)
      end

      fights.each do |fight|
        puts "Create or update fight #{{event_url: fight.event_url, fighter1_id: fight.fighter1_id, fighter2_id: fight.fighter2_id}}"
        # Create or update
        f = Fight.where(event_url: fight.event_url, fighter1_id: fight.fighter1_id, fighter2_id: fight.fighter2_id).first_or_create
        f.update_attributes!(fight.attributes.slice('event_name', 'event_url', 'fighter1_result', 'fighter2_result', 'fought_on', 'location', 'referee', 'result_method', 'result_round', 'result_time'))
      end
    end

    def recent_event_urls
      Fight.where("fought_on between (#{1.week.ago.to_date.to_s(:mdy)}) and (#{1.day.from_now.to_date.to_s(:mdy)})").select(:event_url).distinct.map(&:event_url)
    end

    def existing_event_urls
      Fight.select(:event_url).distinct.map(&:event_url)
    end

    def cached_get_events(new_urls, recent_urls)
      events = load_cached_events
      @a.get(EVENTS_URL) # hack so we can use relative urls at sherdog.com again
      get_urls = new_urls - events.keys # don't download if it's already cached
      get_urls = get_urls + recent_urls # get recent events even if they're cached
      get_urls = get_urls.uniq.compact
      get_urls.each_with_index do |url, i|
        page = @a.get(url)
        save_page page, url
        events[url] = page
        puts "Got url #{i+1} of #{get_urls.size}." if ((i+1) % 20 == 0)
      end
      events
    end

    def load_cached_events
      events = {}
      source_files_glob_pattern = File.join(Rake.application.original_dir, EVENT_CACHE_PATH, '*.html')
      Dir.glob(source_files_glob_pattern) do |file_path|
        page = @a.get "file://#{file_path}"
        file_name = File.basename(file_path, ".html")
        event_url = "/events/#{file_name}"
        puts "Loading cached event page #{file_name}"
        events[event_url] = page
      end
      events
    end

    def save_page page, url
      FileUtils.mkpath EVENT_CACHE_PATH
      file_name = url.gsub('/events/', '') + '.html'
      save_path = File.join(Rake.application.original_dir, EVENT_CACHE_PATH, file_name)
      body = page.body.encode('utf-8', :invalid => :replace, :undef => :replace, :replace => '_')
      puts "Saving #{save_path}"
      File.open(save_path, 'w') { |f| f.write(body) }
    end

    def extract_fights(event_url, event_page)
      return [] if event_page.search('[itemprop="location"]').text.match(/EVENT CANCELED/)
      puts "Extracting fights from #{event_url}"
      fights = extract_main_fights(event_url, event_page)
      fights += extract_sub_fights(event_url, event_page)
    end

    def extract_main_fights(event_url, event_page)
      event_container = event_page.at("[itemtype='http://schema.org/Event']")
      event_name = event_container.at("[itemprop='name']").text
      fought_on = event_container.at("[itemprop='startDate']").attr("content")
      location = event_container.at("[itemprop='location']").text

      fights = []
      fight_containers = event_page.search(".fight")
      fight_containers.each do |fight_con|
        fighter_containers = fight_con.search(".fighter")

        # Ensure exactly 2 fighters
        raise if fighter_containers.size != 2
        
        # Ensure fight result exists
        next if fighter_containers.first.at(".final_result").nil?

        fighter1 = find_or_create_fighter(fighter_containers.first)
        fighter2 = find_or_create_fighter(fighter_containers[1])

        fighter1_result = fighter_containers.first.at(".final_result").text.downcase
        fighter2_result = fighter_containers[1].at(".final_result").text.downcase

        resume_con = fight_con.parent.at(".resume")
        referee = resume_con.at("td:contains('Referee')").text.gsub('Referee', '').strip
        result_method = resume_con.at("td:contains('Method')").text.gsub('Method', '').strip
        result_round = resume_con.at("td:contains('Round')").text.gsub('Round', '').strip
        result_time = resume_con.at("td:contains('Time')").text.gsub('Time', '').strip

        fight = Fight.new(
          event_name: event_name,
          event_url: event_url,
          fighter1_id: fighter1.id,
          fighter1_result: fighter1_result,
          fighter2_id: fighter2.id,
          fighter2_result: fighter2_result,
          fought_on: fought_on,
          location: location,
          referee: referee,
          result_method: result_method,
          result_round: result_round,
          result_time: result_time
        )
        fights << fight
      end
      fights
    end

    def extract_sub_fights(event_url, event_page)
      fights = []

      sub_fight_fighters_container = event_page.at("td.col_two:contains('Fighters')")
      return [] if sub_fight_fighters_container.nil?
      sub_fights_container = sub_fight_fighters_container.parent.parent.parent

      # Ensure table is laid out as expected
      puts sub_fights_container and raise unless sub_fights_container.at("td.col_one:contains('Match')")
      raise unless sub_fights_container.at("td.col_two:contains('Fighters')")
      raise unless sub_fights_container.at("td.col_three:contains('Method')")
      raise unless sub_fights_container.at("td.col_four:contains('Round')")
      raise unless sub_fights_container.at("td.col_five:contains('Time')")

      event_container = event_page.at("[itemtype='http://schema.org/Event']")
      event_name = event_container.at("[itemprop='name']").text
      fought_on = event_container.at("[itemprop='startDate']").attr("content")
      location = event_container.at("[itemprop='location']").text

      fight_containers = sub_fights_container.search('[itemprop="subEvent"]')
      fight_containers.each do |fight_con|
        fighter_containers = fight_con.search(".fighter_result_data")

        # Ensure exactly 2 fighters
        raise if fighter_containers.size != 2

        fighter1 = find_or_create_fighter(fighter_containers.first)
        fighter2 = find_or_create_fighter(fighter_containers[1])

        fighter1_result = fighter_containers.first.at(".final_result").text.downcase
        fighter2_result = fighter_containers[1].at(".final_result").text.downcase

        last_4_cells = fight_con.search("td")[-4, 4]
        result_method = last_4_cells[0].text.strip
        referee = last_4_cells[1].text.strip
        result_round = last_4_cells[2].text.strip
        result_time = last_4_cells[3].text.strip

        fight = Fight.new(
          event_name: event_name,
          event_url: event_url,
          fighter1_id: fighter1.id,
          fighter1_result: fighter1_result,
          fighter2_id: fighter2.id,
          fighter2_result: fighter2_result,
          fought_on: fought_on,
          location: location,
          referee: referee,
          result_method: result_method,
          result_round: result_round,
          result_time: result_time
        )
        fights << fight
      end
      fights
    end

    def find_or_create_fighter(nokogiri_node)
      fighter_name = nokogiri_node.at("[itemprop='name']").text
      fighter_url = nokogiri_node.at("a[itemprop='url']").attr("href")
      fighter1 = Fighter.where(name: fighter_name, url: fighter_url).first_or_create
    end
  end
end
