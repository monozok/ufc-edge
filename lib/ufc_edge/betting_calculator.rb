module UfcEdge

  class BettingCalculator

    def calculate!
      Fight.where("fighter1_betting_line is not null or fighter2_betting_line is not null ").each do |fight|
        expected_return = calculate_expected_return(fight.fighter1_betting_line, fight.fighter1_predicted_chance_to_win)
        if expected_return
          fight.fighter1_betting_return = expected_return
        end
        expected_return = calculate_expected_return(fight.fighter2_betting_line, fight.fighter2_predicted_chance_to_win)
        if expected_return
          fight.fighter2_betting_return = expected_return
        end
        fight.save
      end
    end

    def calculate_expected_return(betting_line, predicted_chance_to_win)
      return nil if betting_line.to_i == 0
      if betting_line.to_i < 0
        win_return = 100 / betting_line.to_f.abs
      else
        win_return = betting_line.to_f / 100
      end
      win_return * predicted_chance_to_win - (1.0 - predicted_chance_to_win)
    end

  end
end