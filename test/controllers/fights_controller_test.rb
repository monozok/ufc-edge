require 'test_helper'

class FightsControllerTest < ActionController::TestCase
  setup do
    @fight = fights(:ufc_170)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fights)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fight" do
    assert_difference('Fight.count') do
      post :create, fight: { event_name: @fight.event_name, event_url: @fight.event_url, fighter1_id: @fight.fighter1_id, fighter1_result: @fight.fighter1_result, fighter1_result_elo: @fight.fighter1_result_elo, fighter2_id: @fight.fighter2_id, fighter2_result: @fight.fighter2_result, fighter2_result_elo: @fight.fighter2_result_elo, fought_on: @fight.fought_on, location: @fight.location, referee: @fight.referee, result_method: @fight.result_method, result_round: @fight.result_round, result_time: @fight.result_time }
    end

    assert_redirected_to fight_path(assigns(:fight))
  end

  test "should show fight" do
    get :show, id: @fight
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fight
    assert_response :success
  end

  test "should update fight" do
    patch :update, id: @fight, fight: { event_name: @fight.event_name, event_url: @fight.event_url, fighter1_id: @fight.fighter1_id, fighter1_result: @fight.fighter1_result, fighter1_result_elo: @fight.fighter1_result_elo, fighter2_id: @fight.fighter2_id, fighter2_result: @fight.fighter2_result, fighter2_result_elo: @fight.fighter2_result_elo, fought_on: @fight.fought_on, location: @fight.location, referee: @fight.referee, result_method: @fight.result_method, result_round: @fight.result_round, result_time: @fight.result_time }
    assert_redirected_to fight_path(assigns(:fight))
  end

  test "should destroy fight" do
    assert_difference('Fight.count', -1) do
      delete :destroy, id: @fight
    end

    assert_redirected_to fights_path
  end
end
